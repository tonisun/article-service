DROP TABLE IF EXISTS article;

CREATE TABLE article (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    description varchar(256) NOT NULL,
    price varchar(18)  NOT NULL
);

INSERT INTO article (description, price) VALUES
('Zahnbürste', '3.45'),
('Klopapier', '1.15'),
('Kaugumie', '0.95'),
('Brot', '1.37'),
('Käse', '6.99'),
('Tomaten', '2.65');