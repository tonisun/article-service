package de.egosanto.ms.article.controller;

import de.egosanto.ms.article.model.Article;
import de.egosanto.ms.article.repository.ArticleRepository;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Article GRUD Controller
 *
 * @author Toni Zeidler
 */
@RestController
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    /**
     * GET /articles get all articles
     *
     * @return
     */
    @GetMapping(value = "/")
    public List<Article> getAllArticles() {
        List<Article> articlesList = new ArrayList<>();
        Iterable<Article> articles =  articleRepository.findAll();

        //if (articles != null) {
            for (Article article : articles)
                articlesList.add(article);

            return articlesList;
        //} else {
          //  return null;
        //}
    }

    /**
     * POST /createArticle creates an article
     * ftom Body Request JSON Object
     * {
     *    "description": "Pen",
     *    "price": "27,11"
     * }
     *
     * @param article as JSON Object
     * @return
     */
    @PostMapping("/createArticle")
    @JsonBackReference
    public ResponseEntity<Article> createArticle(@RequestBody Article article) {
        // save article in db
        return new ResponseEntity<Article>(articleRepository.save(article), HttpStatus.OK);
    }


    /**
     * GET /getArticle(int id) returns JSON representation of article identified by id
     *
     * @param id
     * @return
     */
    @GetMapping("/getArticle")
    public ResponseEntity<Article> getArticle(@RequestParam(value = "id") Long id){
        // get article by id from db
        Optional<Article> articleFromDB = articleRepository.findById(id);

        if (articleFromDB.isPresent())
            return new ResponseEntity<Article>(articleFromDB.get(), HttpStatus.OK);
        else
            return new ResponseEntity("No Article found with this id " + id, HttpStatus.NOT_FOUND);
    }

    /**
     * POST /updateArticle updates the attributes of the given article by JSON Object
     *
     * {
     *    "id": "1"
     *    "description": "Pen",
     *    "price": "27,11"
     * }
     *
     * @param article
     * @return
     */
    @PutMapping("/updateArticle")
    public ResponseEntity<Article> updateArticle(@RequestBody Article article) {
        Optional<Article> articleFromBody = articleRepository.findById(article.getId());

        if (articleFromBody.isPresent()) {
            Article savedArticel = articleRepository.save(article);
            return new ResponseEntity<Article>(savedArticel, HttpStatus.OK);
        }

        return new ResponseEntity("No article to update found with id " + article.getId(), HttpStatus.NOT_FOUND);
    }


    /**
     * DELETE /article Delete an article by id
     *
     * @param id
     * @return
     */
    @DeleteMapping("/deleteArticle")
    public ResponseEntity deleteArticle(@RequestParam(value = "id") Long id) {
        Optional<Article> articleFromDB = articleRepository.findById(id);
        if (articleFromDB.isPresent()) {
            articleRepository.deleteById(id);
            return new ResponseEntity("Article deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity("No Article found with this id " + id, HttpStatus.NOT_FOUND);
        }
    }
}
