package de.egosanto.ms.article.repository;

import de.egosanto.ms.article.model.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Toni Zeidler
 *
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article, Long> {

    List<Article> findAll();


}
