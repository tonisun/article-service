package de.egosanto.ms.article;

import de.egosanto.ms.article.model.Article;
import de.egosanto.ms.article.repository.ArticleRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ArticleServiceApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext configurableApplicationContext =
				SpringApplication.run(ArticleServiceApplication.class, args);

		ArticleRepository articleRepository =
				configurableApplicationContext.getBean(ArticleRepository.class);

		Article article1 = new Article("9-Euro-Ticket", 49.01f);
		articleRepository.save(article1);

		Article article2 = new Article("Zahnbürste", 3.45f);
		articleRepository.save(article2);

		Article article3 = new Article("Klopapier", 3.15f);
		articleRepository.save(article3);

		Article article4 = new Article("Kaugumi", 0.95f);
		articleRepository.save(article4);

		Article article5 = new Article("Brot", 1.37f);
		articleRepository.save(article5);

		Article article6 = new Article("Käse", 6.99f);
		articleRepository.save(article6);

		Article article7 = new Article("Tomaten", 2.65f);
		articleRepository.save(article7);

	}

}
