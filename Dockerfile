# Start with a base image containing Java runtime
FROM openjdk:11-slim as build

# Information around who maintains the image
MAINTAINER egosanto.de

# Add the microservice jar to the container
COPY target/article-service-0.0.1-SNAPSHOT.jar article-service-0.0.1-SNAPSHOT.jar

EXPOSE 8280

# execute the microservice
ENTRYPOINT ["java", "-jar", "/article-service-0.0.1-SNAPSHOT.jar"]


